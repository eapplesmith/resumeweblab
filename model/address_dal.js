var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view address_view as
 select s.*, a.street, a.zip_code from address s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM address;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(address_id, callback) {
    var query = 'SELECT c.*, a.street, a.zip_code FROM address c ' +
        'LEFT JOIN street ca on ca.address_id = c.address_id ' +
        'LEFT JOIN address a on a.address_id = ca.address_id ' +
        'WHERE c.address_id = ?';
    var queryData = [address_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE address
    var query = 'INSERT INTO address (street) VALUES (?)';

    var queryData = [params.street];

    connection.query(query, params.street, function(err, result) {

        // THEN USE THE address_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO street
        var address_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO street (address_id, address_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var addressAddressData = [];
        if (params.address_id.constructor === Array) {
            for (var i = 0; i < params.address_id.length; i++) {
                addressAddressData.push([address_id, params.address_id[i]]);
            }
        }
        else {
            addressAddressData.push([address_id, params.address_id]);
        }

        // NOTE THE EXTRA [] AROUND addressAddressData
        connection.query(query, [addressAddressData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(address_id, callback) {
    var query = 'DELETE FROM address WHERE address_id = ?';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var addressAddressInsert = function(address_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO street (address_id, address_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var addressAddressData = [];
    if (addressIdArray.constructor === Array) {
        for (var i = 0; i < addressIdArray.length; i++) {
            addressAddressData.push([address_id, addressIdArray[i]]);
        }
    }
    else {
        addressAddressData.push([address_id, addressIdArray]);
    }
    connection.query(query, [addressAddressData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.addressAddressInsert = addressAddressInsert;

//declare the function so it can be used locally
var addressAddressDeleteAll = function(address_id, callback){
    var query = 'DELETE FROM street WHERE address_id = ?';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.addressAddressDeleteAll = addressAddressDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE address SET street = ? WHERE address_id = ?';
    var queryData = [params.street, params.address_id];

    connection.query(query, queryData, function(err, result) {
        //delete street entries for this address
        addressAddressDeleteAll(params.address_id, function(err, result){

            if(params.address_id != null) {
                //insert street ids
                addressAddressInsert(params.address_id, params.address_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS address_getinfo;

     DELIMITER //
     CREATE PROCEDURE address_getinfo (_address_id int)
     BEGIN

     SELECT * FROM address WHERE address_id = _address_id;

     SELECT a.*, s.address_id FROM address a
     LEFT JOIN street s on s.address_id = a.address_id AND address_id = _address_id;

     END //
     DELIMITER ;

     # Call the Stored Procedure
     CALL address_getinfo (4);

 */

exports.edit = function(address_id, callback) {
    var query = 'CALL address_getinfo(?)';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};